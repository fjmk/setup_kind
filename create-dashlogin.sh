#!/bin/bash

# ideas for args:
#-server
#-namespace

# your server name goes here
server=https://dashboard.kind.site
# the name of the service account goes here
name=admin-user

# save the current namespace
current_namespace=$(kubectl config view --minify | grep namespace: | awk '{print $2}')
# Enable this line if you’ve use a custom namespace for the user. This will switch the current namespace to the designated one. 
kubectl config set-context $(kubectl config current-context) --namespace=kubernetes-dashboard

secretname=$(kubectl get serviceaccount $name -o jsonpath="{.secrets[0].name}")
ca=$(kubectl get secret/$secretname -o jsonpath='{.data.ca\.crt}' )
token=$(kubectl get secret/$secretname -o jsonpath='{.data.token}' | base64 --decode)
namespace=$(kubectl get secret/$secretname -o jsonpath='{.data.namespace}' | base64 --decode)

echo "
apiVersion: v1
kind: Config
clusters:
- name: default-cluster
  cluster:
certificate-authority-data: ${ca}
server: ${server}
contexts:
- name: default-context
  context:
    cluster: default-cluster
    namespace: ${namespace}
    user: default-user
current-context: default-context
users:
- name: default-user
  user:
    token: ${token}
" > sa.kubeconfig

# set back the old namespace
kubectl config set-context $(kubectl config current-context) --namespace=${current_namespace} >/dev/null
echo "namespace: ${current_namespace}"
